import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  imageUrl = 'https://image.freepik.com/free-vector/set-people-using-modern-technologies_74855-1744.jpg';

  imageUrlSecond = 'https://image.freepik.com/free-vector/javascript-frameworks-concept-illustration_114360-743.jpg';

  imageUrlThird = 'https://image.freepik.com/free-vector/people-using-digital-devices-modern-office_1262-19462.jpg';

  constructor() {
  }

  ngOnInit(): void {

  }



}

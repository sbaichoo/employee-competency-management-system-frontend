import { Component, OnInit } from '@angular/core';
import {EmployeeSkill} from '../../models/employee-skill';
import {SkillSetService} from '../../services/skill-set.service';
import {SkillSetDto} from '../../models/skill-set-dto';
import {newArray} from '@angular/compiler/src/util';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {Employee} from '../../models/employee';
import {Skill} from '../../models/skill';
import {stringify} from 'querystring';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  employeeSkills: EmployeeSkill[];

  skillSet: SkillSetDto[];
  employeeClass: Employee;
  skillClass: Skill;
  bool: any;
  position = '';
  department = '';
  employee = '';
  skill = '';
  departments: string[];
  positions: string[];
  myControl = new FormControl();

  filteredOptions: Observable<string[]>;
  private search: string[];
  boole: any;
  dept = '';
  emp = '';
  pos = '';
  name = '';
  constructor(private skillSetService: SkillSetService) {
    this.skillSet = [];
    this.employeeSkills = [];
  }

  ngOnInit(): void {
    this.bool = true;
    this.boole = true;
    this.departments = ['', 'General operations', 'Human Resources', 'MU .Net DU', 'MU Java DU', 'MU iPension DU',
      'Finance and Admin'];
    this.positions = ['', 'Engineer', 'Senior Project Manager', 'Senior Consultant', 'Architect', 'Senior Specialist', 'Admin',
      'Project Manager', 'Expert'];
  }

  findSkillSetsByCriteria(position: string, department: string, employee: string, skill: string) {
    this.skillSetService.findSkillSetsByCriteria(position, department, employee, skill).subscribe(
      data => {this.employeeSkills = data; },
         error => { console.log(error); }
    );
  }
  searchSkillSet(position: string, department: string, employee: string, skill: string) {
    if (this.employeeSkills.length === 0) {
      this.bool = false;
    }
    this.findSkillSetsByCriteria(position, department, employee, skill);
  }

  unHide() {
    this.boole = ! this.boole;
  }
}

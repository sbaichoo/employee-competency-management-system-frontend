import { Component, OnInit } from '@angular/core';
import {EmployeeSkill} from '../../models/employee-skill';
import {EmployeeSkillService} from '../../services/employee-skill.service';

@Component({
  selector: 'app-search-skill',
  templateUrl: './search-skill.component.html',
  styleUrls: ['./search-skill.component.css']
})
export class SearchSkillComponent implements OnInit {

  employeeskills: EmployeeSkill[];

  page: any = 1;

  pages: Array<number>;

  imageUrl = 'https://image.freepik.com/free-vector/set-people-using-modern-technologies_74855-1744.jpg';

  imageUrlSecond = 'https://image.freepik.com/free-vector/javascript-frameworks-concept-illustration_114360-743.jpg';

  imageUrlThird = 'https://image.freepik.com/free-vector/people-using-digital-devices-modern-office_1262-19462.jpg';

  constructor(private employeeSkillService: EmployeeSkillService) {
    this.employeeskills = new Array();
  }

  ngOnInit() {
    this.getAllEmployeesSkills();
  }

  getAllEmployeesSkills() {
    this.employeeSkillService.getAllEmployeesSkills(this.page).subscribe(
      data => {
        this.employeeskills = data['content'];
        this.pages = new Array(data['totalPages']);
      },
      (error) => {console.log(error.error.message);
      }
    );
  }

  setActive(i: number, event) {
    event.preventDefault();
    this.page = i;
    this.getAllEmployeesSkills();
  }

}

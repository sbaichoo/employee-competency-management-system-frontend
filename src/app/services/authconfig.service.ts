import { Injectable } from '@angular/core';
import { AuthConfig, NullValidationHandler, OAuthService } from 'angular-oauth2-oidc';
import { filter } from 'rxjs/operators';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthConfigService {

  private _decodedAccessToken: any;
  private _decodedIDToken: any;
  private name: any;
  private roles: any;
  private expiredIn: number | string | Date;
  private nowLocal: number | string | Date;
  get decodedAccessToken() { return this._decodedAccessToken; }
  get decodedIDToken() { return this._decodedIDToken; }

  constructor(
    private readonly oauthService: OAuthService,
    private readonly authConfig: AuthConfig,
    private router: Router
  ) {}

  async initAuth(): Promise<any> {
    return new Promise((resolveFn, rejectFn) => {
      // setup oauthService
      this.oauthService.configure(this.authConfig);
      this.oauthService.setStorage(localStorage);
      this.oauthService.tokenValidationHandler = new NullValidationHandler();

      // subscribe to token events
      this.oauthService.events
        .pipe(filter((e: any) => {
          return e.type === 'token_received';
        }))
        .subscribe(() => this.handleNewToken());
      // disabling keycloak for now
      // resolveFn();
      // continue initializing app or redirect to login-page

      this.oauthService.loadDiscoveryDocumentAndLogin().then(isLoggedIn => {
        if (isLoggedIn) {
          this.oauthService.setupAutomaticSilentRefresh();
          resolveFn();
        } else {
          this.oauthService.initImplicitFlow();
          rejectFn();
        }
      });

    });
  }

  private handleNewToken() {
    this._decodedAccessToken = this.oauthService.getAccessToken();
    this._decodedIDToken = this.oauthService.getIdToken();
  }

  getName() {
    if (this.oauthService.getIdentityClaims()) {
      this.name = this.oauthService.getIdentityClaims()['name'];
      console.log('LOGGED IN WITH ' + this.name);
    }
  }

  getUsername(): string  {
    if (this.oauthService.getIdentityClaims()) {
      return this.oauthService.getIdentityClaims()['preferred_username'];
      console.log('VISA:' + this.oauthService.getIdentityClaims()['preferred_username']);
    }
  }

  public isValidAccessToken() {
    return this.oauthService.hasValidAccessToken();
  }

  public getMainRole(): string {
    this.roles = this.oauthService.getIdentityClaims()['authorities'];

    if (this.roles.includes('ROLE_ADMIN')) {
      return 'ADMIN';
    }

    if (this.roles.includes('ROLE_MANAGER')) {
      return 'MANAGER';
    }

    return 'USER';
  }

  getExpiredTokenDate(): Date {
    this.expiredIn = this.oauthService.getAccessTokenExpiration();

    this.nowLocal = new Date().getTime();

    return new Date(this.nowLocal + this.expiredIn);

  }

  getAuthorities(): Array<any> {
    return this.roles = this.oauthService.getIdentityClaims()['authorities'];
  }

  checkLoginRequest(): boolean {
    if (this.getAuthorities() == null || this.getAuthorities().length === 0) {
      return false;
    }
    return true;
  }

  redirect(url: string): void {
    this.router.navigateByUrl(url);
  }



}

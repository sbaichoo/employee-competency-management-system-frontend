import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { EmployeeSkill } from '../models/employee-skill';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import {environment} from '../../environments/environment';

const apiUrl = '/skillemployee';

@Injectable({
  providedIn: 'root'
})
export class EmployeeSkillService {

  private baseURL = environment.REST_API_URL + apiUrl;

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(private httpClient: HttpClient) { }

  getAllEmployeesSkills(page: number): Observable<EmployeeSkill[]> {
    return this.httpClient.get<EmployeeSkill[]>(this.baseURL + '?page=' + page , this.httpOptions);
  }

  getEmployeeSkillDetails(id: number): Observable<EmployeeSkill> {
    return this.httpClient.get<EmployeeSkill>(this.baseURL + '/' + id, this.httpOptions);
  }

  getAllEmployeesSkillsByDepartment(name: string): Observable<EmployeeSkill[]> {
    return this.httpClient.get<EmployeeSkill[]>(this.baseURL + '/department/' + name + '?limit=10', this.httpOptions);
  }





}

import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Skill} from '../models/skill';
import {Observable} from 'rxjs';


const headers = new HttpHeaders().set('Content-Type', 'application/json');
const apiUrl = '/skills';

@Injectable({
  providedIn: 'root'
})
export class SkillService {

  private baseURL = environment.REST_API_URL + apiUrl;

  constructor(private httpClient: HttpClient) { }

  getAllSkills(): Observable<Skill[]> {
    return this.httpClient.get<Skill[]>(this.baseURL + '?page=1&size=100', {headers});
  }

  getSkillByName(name: string): Observable<Skill> {
    return this.httpClient.get<Skill>(this.baseURL + '/name/' + name.split(' ').join('%20'));
  }


}

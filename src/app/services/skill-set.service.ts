import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';
import {SkillSetDto} from '../models/skill-set-dto';
import {EmployeeSkill} from '../models/employee-skill';

const headers = new HttpHeaders().set('Content-Type', 'application/json');
const apiUrl = '/skillsets';

@Injectable({
  providedIn: 'root'
})
export class SkillSetService {

  private baseURL = environment.REST_API_URL + apiUrl;

  constructor(private httpClient: HttpClient) {
  }

  updateSkill(id: number, level: string): Observable<SkillSetDto> {
    return this.httpClient.put<SkillSetDto>(this.baseURL + '/level/' + id + '/' + level, {headers});
  }

  getSkillSetById(id: number): Observable<SkillSetDto> {
    return this.httpClient.get<SkillSetDto>(this.baseURL + '/' + id);
  }

  getSkillSetByEmployeeId(id: number): Observable<SkillSetDto[]> {
    return this.httpClient.get<SkillSetDto[]>(this.baseURL + '/employee/' + id, {headers});
  }

  softDeleteSkillSet(skillSetId: number): Observable<SkillSetDto> {
    return this.httpClient.put<SkillSetDto>(this.baseURL + '/delete/' + skillSetId, {headers});
  }

  addNewSkillSet(employeeID: number, skillID: number, level: string): Observable<SkillSetDto> {
    return this.httpClient.post<SkillSetDto>(this.baseURL + '/' + employeeID + '/' + skillID + '/' + level, {headers});
  }

  findSkillSetsByCriteria(position: string, department: string, employee: string, skill: string): Observable<EmployeeSkill[]> {
    const params = new HttpParams()
      .set('position', position)
      .set('department', department)
      .set('employee', employee)
      .set('skill', skill);
    return this.httpClient.get<EmployeeSkill[]>(this.baseURL + '/search', {headers, params});
  }
}

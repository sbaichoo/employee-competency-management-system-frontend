import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Employee} from '../models/employee';
import {Observable, of} from 'rxjs';
import {catchError, map, tap} from 'rxjs/operators';

const apiUrl = '/employees';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  private baseURL = environment.REST_API_URL + apiUrl;

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(private httpClient: HttpClient) { }

  getEmployeeById(id: number): Observable<Employee> {
    return this.httpClient.get<Employee>(this.baseURL + '/' + id, this.httpOptions)
      .pipe(
        tap(_ => console.log(`fetched employee id=${id}`)),
        catchError(this.handleError<Employee>(`getEmployeeById id=${id}`))
      );
  }

  getEmployeeByUserName(visa: string): Observable<Employee> {
    return this.httpClient.get<Employee>(this.baseURL + '/search?visa=' + visa, this.httpOptions);
      // .pipe(map(resp => resp.id ));
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

}

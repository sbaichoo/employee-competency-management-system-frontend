import { AuthConfig } from 'angular-oauth2-oidc';
import {environment} from '../../../environments/environment';

export const authConfig: AuthConfig = {

    issuer: environment.keycloak.issuer,
    redirectUri: environment.keycloak.redirectUri,
    clientId: environment.keycloak.clientId,
    dummyClientSecret: environment.keycloak.dummyClientSecret,
    scope: environment.keycloak.scope,
    responseType: environment.keycloak.responseType,
    disableAtHashCheck: environment.keycloak.disableAtHashCheck,
    showDebugInformation: environment.keycloak.showDebugInformation,
    requireHttps: environment.keycloak.requireHttps
  };


export class OAuthModuleConfig {
  resourceServer: OAuthResourceServerConfig = {sendAccessToken: true, allowedUrls: ['http://localhost:8080/api']};
}

export class OAuthResourceServerConfig {
  /**
   * Urls for which calls should be intercepted.
   * If there is an ResourceServerErrorHandler registered, it is used for them.
   * If sendAccessToken is set to true, the access_token is send to them too.
   */
  allowedUrls?: Array<string>;
  sendAccessToken = true;
}

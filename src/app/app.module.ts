import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AccessDeniedComponent } from './error/access-denied/access-denied.component';
import { NotFoundComponent } from './error/not-found/not-found.component';
import {AuthConfigModule} from './core/auth/auth.config.module';
import {AppMaterialModule} from './shared/app.material.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FlexLayoutModule} from '@angular/flex-layout';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { HomeComponent } from './home/home/home.component';
import {ProfileRoutingModule} from './profiles/profile-routing.module';
import {ProfileModule} from './profiles/profile.module';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import {AuthConfig, OAuthModule} from 'angular-oauth2-oidc';
import { SearchSkillComponent } from './search/search-skill/search-skill.component';
import { SearchComponent } from './search/search/search.component';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatButtonToggleModule } from '@angular/material/button-toggle';


@NgModule({
  declarations: [
    AppComponent,
    AccessDeniedComponent,
    NotFoundComponent,
    HomeComponent,
    SearchSkillComponent,
    SearchComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    AppRoutingModule,
    AuthConfigModule,
    AppMaterialModule,
    BrowserAnimationsModule,
    FormsModule,
    FlexLayoutModule,
    ProfileRoutingModule,
    ProfileModule,
    MDBBootstrapModule.forRoot(),
    OAuthModule.forRoot({
      resourceServer: {
        allowedUrls: ['http://localhost:8080/api'],
        sendAccessToken: true
      }
    }),
    ReactiveFormsModule,
    MatAutocompleteModule,
    MatButtonToggleModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

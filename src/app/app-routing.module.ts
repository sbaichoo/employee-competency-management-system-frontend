import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {NotFoundComponent} from './error/not-found/not-found.component';
import {AccessDeniedComponent} from './error/access-denied/access-denied.component';
import {HomeComponent} from './home/home/home.component';
import {ProfileComponent} from './profiles/profile/profile.component';
import {ProfileRoutingModule} from './profiles/profile-routing.module';
import {ProfileSharedComponent} from './profiles/profile-shared.component';
import {ProfileSearchComponent} from './profiles/profile-search/profile-search.component';
import {SkillEditComponent} from './profiles/skill-edit/skill-edit.component';
import {SkillAddComponent} from './profiles/skill-add/skill-add.component';
import {SearchSkillComponent} from './search/search-skill/search-skill.component';
import {SearchComponent} from './search/search/search.component';

const routes: Routes = [
  { path: '', pathMatch: 'full',  children: [
      {path: '', component: HomeComponent}]},
  // { path: '404', component: NotFoundComponent },
  // { path: '401', component: AccessDeniedComponent },
  // { path: '**', redirectTo: '404' },

  {path: 'search/all', component: SearchSkillComponent },
  {path: 'search', component: SearchComponent },
  {path: 'profile', component: ProfileSharedComponent,
    children: [
      {path: '', component: ProfileComponent},
      {path: 'search', component: ProfileSearchComponent},
      {path: 'skillset/edit', component: SkillEditComponent},
      {path: 'skillset/add', component: SkillAddComponent},
    ]},

];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: 'enabled'
})],
  exports: [RouterModule]
})
export class AppRoutingModule { }

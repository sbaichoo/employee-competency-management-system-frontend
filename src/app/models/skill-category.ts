export class SkillCategory {
  id: number;
  categoryName: string;
  skillAreaName: string;
}

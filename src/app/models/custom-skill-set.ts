import { Employee } from './employee';

export class CustomSkillSet {

  id: number;
  employee: Employee;
  name: string;
  skillLevel: string;
  skillCategory: string;
}

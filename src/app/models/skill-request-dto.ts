import {SkillCategory} from './skill-category';
import {EmployeeSimplified} from './employee-simplified';

export class SkillRequestDto {
  id: number;
  name: string;
  description: string;
  status: string;
  skillCategory: SkillCategory;
  employeeDtoSimplified: EmployeeSimplified;
}

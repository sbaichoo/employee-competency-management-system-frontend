import {SkillRequest} from './skill-request';

export interface EmployeeSkillRequest {
  id: number;
  name: string;
  skillRequestModelList: SkillRequest[];
}

import {Employee} from './employee';
import {Skill} from './skill';

export class SkillSetDto {
  skillEmployeeId: number;
  employee: Employee;
  skill: Skill;
  skillLevel: string;
}

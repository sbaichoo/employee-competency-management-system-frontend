import {SkillCategory} from './skill-category';

export class SkillRequest {

  id: number;
  name: string;
  description: string;
  status: string;
  skillCategory: SkillCategory[];

}

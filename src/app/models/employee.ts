
export class Employee {
  id: number;
  firstName: string;
  lastName: string;
  visa: string;
  email: string;
  positionName: string;
  departmentName: string;
}

import {SkillCategory} from './skill-category';

export class Skill {
  id: number;
  name: string;
  description: string;
  skillCategory: SkillCategory;
}

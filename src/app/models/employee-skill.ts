import { SkillSet } from './skill-set';

export class EmployeeSkill {

    name: string;
    positionName: string;
    visa: string;
    departmentName: string;
    skillSetModels: SkillSet[];

}


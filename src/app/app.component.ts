import {Component, OnInit} from '@angular/core';
import {OAuthService} from 'angular-oauth2-oidc';
import {AuthConfigService} from './services/authconfig.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  constructor(private readonly oauthService: OAuthService, private authConfigService: AuthConfigService) { }

  ngOnInit() {
    this.authConfigService.getName();
  }

  logout() {
    this.oauthService.logOut();
  }

}


import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogSkillDeleteComponent } from './dialog-skill-delete.component';

describe('DialogSkillDeleteComponent', () => {
  let component: DialogSkillDeleteComponent;
  let fixture: ComponentFixture<DialogSkillDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogSkillDeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogSkillDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

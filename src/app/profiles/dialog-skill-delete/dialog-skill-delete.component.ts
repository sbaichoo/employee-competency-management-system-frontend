import {Component, Inject, OnInit} from '@angular/core';
import {SkillSetService} from '../../services/skill-set.service';
import {SkillSetDto} from '../../models/skill-set-dto';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-dialog-skill-delete',
  templateUrl: './dialog-skill-delete.component.html',
  styleUrls: ['./dialog-skill-delete.component.css']
})
export class DialogSkillDeleteComponent implements OnInit {
  private skillSet: SkillSetDto;

  constructor(private skillSetService: SkillSetService,  @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
  }

  deleteSkill(): void {
    this.softDeleteSkillSet(this.data.id);
  }

  softDeleteSkillSet(skillSetId: number) {
    this.skillSetService.softDeleteSkillSet(skillSetId).subscribe(
      data => {
        this.skillSet = data;
        // console.log(data);
      }, error => {console.log(error); }
    );
  }
}

import {Component, OnInit, Input, Injectable} from '@angular/core';
import {Employee} from '../models/employee';
import {ActivatedRoute, Router} from '@angular/router';
import {EmployeeService} from '../services/employee.service';
import {AuthConfigService} from '../services/authconfig.service';

@Component({
  selector: 'app-profile-shared',
  templateUrl: './profile-shared.component.html',
  styleUrls: ['./profile-shared.component.css']
})

@Injectable({
  providedIn: 'root'
})
export class ProfileSharedComponent implements OnInit {

  @Input() employee: Employee;

  visa: string;

  id: number;
  idUser: number;
  constructor(private route: ActivatedRoute,
              private router: Router, private employeeService: EmployeeService,
              private authConfigService: AuthConfigService) {
    this.employee = new Employee();
  }

  async ngOnInit() {

    this.visa = this.authConfigService.getUsername();

    this.idUser = await this.getEmployeeIdByUserName(this.visa);

    this.getEmployeeById(this.idUser);
  }


  getEmployeeById(id: number) {
    this.employeeService.getEmployeeById(id).subscribe(
      data => {
        this.employee = data;
        // console.log(data);
      }, error => {console.log(error); }
    );
  }

  async getEmployeeIdByUserName(visa: string): Promise<number> {
    return await this.employeeService.getEmployeeByUserName(visa).toPromise().then(a => a[0].id);
  }

  async getEmployeeIdFromCurrentUser(): Promise<number> {
    this.visa = this.authConfigService.getUsername();
    this.idUser = await this.getEmployeeIdByUserName(this.visa);
    return this.idUser;
  }


}

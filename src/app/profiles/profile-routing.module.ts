import {Injectable, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {ProfileComponent} from './profile/profile.component';
import {ProfileSearchComponent} from './profile-search/profile-search.component';
import {SkillEditComponent} from './skill-edit/skill-edit.component';
import {SkillAddComponent} from './skill-add/skill-add.component';
import {ProfileSharedComponent} from './profile-shared.component';

const profileRoutes: Routes = [
  {path: 'profile', component: ProfileSharedComponent,
    children: [
      {path: '', component: ProfileComponent},
      {path: 'search', component: ProfileSearchComponent},
      {path: 'skillset/edit', component: SkillEditComponent},
      {path: 'skillset/add', component: SkillAddComponent},
    ]},
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(profileRoutes)
  ],
  exports: [
    RouterModule
  ]
})
@Injectable()
export class ProfileRoutingModule { }

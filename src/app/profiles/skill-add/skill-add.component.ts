import { Component, OnInit } from '@angular/core';
import {SkillService} from '../../services/skill.service';
import {FormControl} from '@angular/forms';
import { Observable } from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import {Skill} from '../../models/skill';
import {SkillCategory} from '../../models/skill-category';
import {DialogSkillEditComponent} from '../dialog-skill-edit/dialog-skill-edit.component';
import { MatDialog } from '@angular/material/dialog';
import {Router} from '@angular/router';
import {DialogSkillAddComponent} from '../dialog-skill-add/dialog-skill-add.component';
import { SkillEditComponent } from '../skill-edit/skill-edit.component';
import {ProfileSharedComponent} from '../profile-shared.component';

@Component({
  selector: 'app-skill-add',
  templateUrl: './skill-add.component.html',
  styleUrls: ['./skill-add.component.css']
})
export  class SkillAddComponent implements OnInit {
  option: any = 'Linq';

  bool: any = true;

  skill: Skill;

  skillCategory: SkillCategory;
  private id: number;

  constructor(private skillService: SkillService, private dialog: MatDialog, private skillEditComponent: SkillEditComponent,
              private profileSharedComponent: ProfileSharedComponent) {
    this.skill = new Skill();
    this.skillCategory = new SkillCategory();
  }

  myControl = new FormControl();

  filteredOptions: Observable<string[]>;
  private search: string[];
  async ngOnInit(): Promise<void> {
    this.id = await this.profileSharedComponent.getEmployeeIdFromCurrentUser();

    this.filterSkill().then();

    this.getSkillByName(this.option);

    this.bool = true;


  }

  getAllSkills() {
    return this.skillService.getAllSkills().toPromise().then(data => data.map(a => a.name));
  }
  async filterSkill() {
    this.search = await this.getAllSkills().then(data => data);

    this.filteredOptions = this.myControl.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value)),
        map(value => value.slice(0, 3))
      );
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.search.filter(option => option.toLowerCase().includes(filterValue));
  }

  enableAddSkill(option: string) {
    this.bool = false;
    this.getSkillByName(option);
  }

  getSkillByName(name: string) {
    return this.skillService.getSkillByName(name).subscribe(
      data => {
        this.skill = data;
        this.skillCategory = data.skillCategory;
      }
    );
  }

  AddSkill(id: number) {
    this.dialog.open(DialogSkillAddComponent,   {
      height: '400px',
      width: '400px',
      data: {idSkill: id, id}
    });
    this.dialog.afterAllClosed.subscribe(() => this.skillEditComponent.getEmployeeSkillDetails(this.id));
  }
}

import {Component, Injectable, OnInit} from '@angular/core';
import {EmployeeSkillService} from '../../services/employee-skill.service';
import { MatDialog } from '@angular/material/dialog';
import {SkillSet} from '../../models/skill-set';
import {DialogSkillEditComponent} from '../dialog-skill-edit/dialog-skill-edit.component';
import {ProfileSharedComponent} from '../profile-shared.component';
import {DialogSkillDeleteComponent} from '../dialog-skill-delete/dialog-skill-delete.component';
import {Router} from '@angular/router';

@Component({
  selector: 'app-skill-edit',
  templateUrl: './skill-edit.component.html',
  styleUrls: ['./skill-edit.component.css']
})
@Injectable({
  providedIn: 'root'
})
export class SkillEditComponent implements OnInit {
  id: number;
  private employeeSkill: SkillSet[];

  constructor(private employeeSkillService: EmployeeSkillService,
              private dialog: MatDialog, private router: Router,
              private profileSharedComponent: ProfileSharedComponent) { }

  async ngOnInit(): Promise<void>  {

    this.id = await this.profileSharedComponent.getEmployeeIdFromCurrentUser();

    this.getEmployeeSkillDetails(this.id);

  }

  getEmployeeSkillDetails(id: number) {
    this.employeeSkillService.getEmployeeSkillDetails(id).subscribe(
      data => {
        this.employeeSkill = data.skillSetModels;

        // console.log(data.skillSetModels);
      }, error => {console.log(error); }
    );
  }

  openDialogEdit(id: number) {
    this.dialog.open(DialogSkillEditComponent,   {
      height: '400px',
      width: '400px',
      data: {idSkill: id, id}
    });
    this.dialog.afterAllClosed.subscribe(() => this.getEmployeeSkillDetails(this.id));
  }
  openDialogDelete(id: number) {
    this.dialog.open(DialogSkillDeleteComponent, {
      height: '300px',
      width: '250px',
      data: {idSkill: id, id}
    });
    this.dialog.afterAllClosed.subscribe( () => this.getEmployeeSkillDetails(this.id));
  }

  goToSkillAdd() {
    this.router.navigate(['../profile/skillset/add']);
  }
}

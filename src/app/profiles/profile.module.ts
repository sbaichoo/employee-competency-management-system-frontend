import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileRoutingModule } from './profile-routing.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AppMaterialModule} from '../shared/app.material.module';
import {FlexModule} from '@angular/flex-layout';
import {RouterModule} from '@angular/router';
import { ProfileComponent } from './profile/profile.component';
import { ProfileSearchComponent } from './profile-search/profile-search.component';
import { SkillEditComponent } from './skill-edit/skill-edit.component';
import { SkillAddComponent } from './skill-add/skill-add.component';
import { ProfileSharedComponent } from './profile-shared.component';
import { DialogSkillEditComponent } from './dialog-skill-edit/dialog-skill-edit.component';
import { DialogSkillDeleteComponent } from './dialog-skill-delete/dialog-skill-delete.component';
import {IconsModule} from 'angular-bootstrap-md';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { DialogSkillAddComponent } from './dialog-skill-add/dialog-skill-add.component';

@NgModule({
  // tslint:disable-next-line:max-line-length
  declarations: [ProfileComponent, ProfileSearchComponent, SkillEditComponent, SkillAddComponent, ProfileSharedComponent, DialogSkillEditComponent, DialogSkillDeleteComponent, DialogSkillAddComponent],
  imports: [
    CommonModule,
    FormsModule,
    AppMaterialModule,
    FlexModule,
    RouterModule,
    ReactiveFormsModule,
    ProfileRoutingModule,
    IconsModule,
    MatAutocompleteModule
  ],
  exports : [
  ],
  providers: [
  ],
  entryComponents: []
})
export class ProfileModule { }




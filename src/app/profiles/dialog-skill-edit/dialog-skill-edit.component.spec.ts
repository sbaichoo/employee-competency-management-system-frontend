import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogSkillEditComponent } from './dialog-skill-edit.component';

describe('DialogSkillEditComponent', () => {
  let component: DialogSkillEditComponent;
  let fixture: ComponentFixture<DialogSkillEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogSkillEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogSkillEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {Component, Inject, OnInit} from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import {SkillSetDto} from '../../models/skill-set-dto';
import {SkillSetService} from '../../services/skill-set.service';
import {SkillSet} from '../../models/skill-set';

@Component({
  selector: 'app-dialog-skill-edit',
  templateUrl: './dialog-skill-edit.component.html',
  styleUrls: ['./dialog-skill-edit.component.css']
})
export class DialogSkillEditComponent implements OnInit {

  level = 'Expert';

  employeeSkill: SkillSet[];
  skillSet: SkillSetDto;
  constructor(private skillSetService: SkillSetService,
              @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
  }

  saveLevel(level: string) {
    this.updateSkill(this.data.id, level);
  }

  updateSkill(id: number, level: string) {
    this.skillSetService.updateSkill(id, level).subscribe(
      data => {
        this.skillSet = data['SkillSetDto'];
        // console.log(data);
      }, error => {console.log(error); }
    );
  }

}

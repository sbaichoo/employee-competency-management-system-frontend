import {Component, Inject, OnInit} from '@angular/core';
import {SkillSet} from '../../models/skill-set';
import {SkillSetDto} from '../../models/skill-set-dto';
import {SkillSetService} from '../../services/skill-set.service';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import {ProfileSharedComponent} from '../profile-shared.component';

@Component({
  selector: 'app-dialog-skill-add',
  templateUrl: './dialog-skill-add.component.html',
  styleUrls: ['./dialog-skill-add.component.css']
})
export class DialogSkillAddComponent implements OnInit {
  level = 'Expert';

  employeeID: number;

  skillSet: SkillSetDto;
  constructor(private skillSetService: SkillSetService, private profileSharedComponent: ProfileSharedComponent,
              @Inject(MAT_DIALOG_DATA) public data: any) {
    this.skillSet = new SkillSetDto();
  }

  async ngOnInit(): Promise<void> {
    this.employeeID =  await this.profileSharedComponent.getEmployeeIdFromCurrentUser();
  }

  saveSkillAdded(level: string) {
    this.addNewSkillSet(this.employeeID, this.data.id, level);
  }

  addNewSkillSet(employeeID: number, skillID: number, level: string) {
    this.skillSetService.addNewSkillSet(employeeID, skillID, level).subscribe(
      data => {
        this.skillSet = data;
      }
    );
  }
}

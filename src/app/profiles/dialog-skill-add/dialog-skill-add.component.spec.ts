import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogSkillAddComponent } from './dialog-skill-add.component';

describe('DialogSkillAddComponent', () => {
  let component: DialogSkillAddComponent;
  let fixture: ComponentFixture<DialogSkillAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogSkillAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogSkillAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

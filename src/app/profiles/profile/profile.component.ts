import { Component, OnInit } from '@angular/core';
import {ProfileSharedComponent} from '../profile-shared.component';
import {EmployeeService} from '../../services/employee.service';
import {EmployeeSkillService} from '../../services/employee-skill.service';
import {Employee} from '../../models/employee';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  private employee: Employee;
  private employeeSkill: any[];
  private id: number;

  constructor(private profileSharedComponent: ProfileSharedComponent,
              private employeeService: EmployeeService,
              private employeeSkillService: EmployeeSkillService) {
    this.employee = new Employee();
    this.employeeSkill = new Array();
  }

  async ngOnInit(): Promise<void> {

    this.id = await this.profileSharedComponent.getEmployeeIdFromCurrentUser();

    this.getEmployeeSkillDetails(this.id);

    this.getEmployeeById(this.id);
  }

  getEmployeeById(id: number) {
    this.employeeService.getEmployeeById(id).subscribe(
      data => {
        this.employee = data;
      }
    );
  }

  getEmployeeSkillDetails(id: number) {
    this.employeeSkillService.getEmployeeSkillDetails(id).subscribe(
      data => {
        this.employeeSkill = data.skillSetModels;
        // console.log(data);
      }, error => {console.log(error); }
    );
  }

}
